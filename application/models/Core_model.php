<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Core_model extends CI_Model
{

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  /**
   * ===========================================================
   * DATATABLES SERVER-SIDE METHODS
   * ===========================================================
   * 
   * function all()
   * function _get_datatables_query()
   * function get_datatables()
   * function count_filtered()
   * function count_all()
   */

  /**
   * @return tables
   */
  function all()
  {
    return $this->db->get($this->table);
  }

  /**
   * @param date string (null)
   * @param start_date string (null)
   * @param end_date string (null)
   */
  function _get_datatables_query($date = null, $start_date = null, $end_date = null)
  {
    $this->db->from($this->table);
    // Check if variable join is TRUE or FALSE
    if (!$this->join) {
      $this->db->where('has_delete !=', 1);
    } else {
      $this->db->join($this->table_join, $this->table_join . '.' . $this->idq_fk . '=' . $this->table . '.' . $this->idq);
      $this->db->where($this->table . '.has_delete !=', 1);
    }
    if ($start_date && $end_date) {
      $this->db->where('DATE('. $date .') >=', $start_date);
      $this->db->where('DATE('. $date .') <=', $end_date);
    }

    // DATATABLE SEARCH
    $i = 0;
    foreach ($this->column_search as $item) {
      if ($_POST['search']['value']) {
        if ($i === 0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        if (count($this->column_search) - 1 == $i) {
          $this->db->group_end();
        }

        $i++;
      }
    }

    // DATATABLE ORDER
    if (isset($_POST['order'])) {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } else {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }

  /**
   * @param date string (null)
   * @param start_date string (null)
   * @param end_date string (null)
   * 
   * @return result
   */
  function get_datatables($date = null, $start_date = null, $end_date = null)
  {
    $this->_get_datatables_query($date, $start_date, $end_date);

    if ($_POST['length'] != -1) {
      $this->db->limit($_POST['length'], $_POST['start']);
    }

    $query = $this->db->get();
    return $query->result();
  }

  /**
   * @return num_rows
   */
  function count_filtered()
  {
    $this->_get_datatables_query();
    $query = $this->db->get();
    return $query->num_rows();
  }

  /**
   * @return count_all_results
   */
  function count_all()
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }

  /** 
   * ===========================================================
   * CRUD METHODS
   * ===========================================================
   * 
   * function get_by_row($table, $condition = null, $table_join = null, $id = null, $idq = null, $idq_fk = null, $join = false)
   * function get_by_array($table, $condition = null, $table_join = null, $id = null, $idq = null, $idq_fk = null, $join = false)
   * function get_join($table, $column, $table_join, $idq, $idq_fk)
   * function create($table, $data)
   * function create_detail($table_join, $data)
   * function update($table, $where, $data)
   * function delete($table, $where, $data)
   */

  /**
   * @param table string
   * @param condition array (null)
   * @param table_join string (null)
   * @param id string (null)
   * @param idq string (null)
   * @param idq_fk string (null)
   * @param join boolean (false)
   * 
   * @return row
   */
  function get_by_row($table, $condition = null, $table_join = null, $id = null, $idq = null, $idq_fk = null, $join = false)
  {
    $this->db->from($table);
    if ($condition) {
      $this->db->where($condition);
    }
    // Check if variable join is TRUE or FALSE
    if (!$join) {
      $this->db->where('has_delete !=', 1);
    } else {
      $this->db->join($table_join, $table_join . '.' . $idq_fk . '=' . $table . '.' . $idq);
      $this->db->where($table . '.'. $idq .'='. $id);
      $this->db->where($table . '.has_delete !=', 1);
    }

    $query = $this->db->get();
    return $query->row();
  }

  /**
   * @param table string
   * @param condition array (null)
   * @param table_join string (null)
   * @param id string (null)
   * @param idq string (null)
   * @param idq_fk string (null)
   * @param join boolean (false)
   * 
   * @return row
   */
  function get_by_array($table, $condition = null, $table_join = null, $id = null, $idq = null, $idq_fk = null, $join = false)
  {
    $this->db->from($table);
    if ($condition) {
      $this->db->where($condition);
    }
    // Check if variable join is TRUE or FALSE
    if (!$join) {
      $this->db->where('has_delete !=', 1);
    } else {
      $this->db->join($table_join, $table_join . '.' . $idq_fk . '=' . $table . '.' . $idq);
      $this->db->where($table .'.'. $idq .'='. $id);
      $this->db->where($table .'.has_delete !=', 1);
    }
    
    $query = $this->db->get();
    return $query->result_array();
  }

  /**
   * @param table string
   * @param column string
   * @param table_join string
   * @param idq string
   * @param idq_fk string
   * 
   * @return result_array
   */
  function get_distinct_join($table, $column, $table_join, $idq, $idq_fk)
  {
    $this->db->select('DISTINCT('. $table .'.'. $column .') as '. $column .'');
    $this->db->from($table);
    $this->db->join($table_join, $table_join .'.'. $idq_fk .'='. $table .'.'. $idq);
    $this->db->where($table .'.has_delete != 1');

    $query = $this->db->get();
    return $query->result_array();
  }

  /**
   * @param table string
   * @param data array
   * 
   * @return id
   */
  function create($table, $data)
  {
    $this->db->insert($table, $data);
    return $this->db->insert_id();
  }

  /**
   * @param table_join string
   * @param data array
   * 
   * @return id
   */
  function create_detail($table_join, $data)
  {
    $this->db->insert($table_join, $data);
    return $this->db->insert_id();
  }

  /**
   * @param table string
   * @param where array
   * @param data array
   * 
   * @return affected_rows
   */
  function update($table, $where, $data)
  {
    $this->db->update($table, $data, $where);
    return $this->db->affected_rows();
  }

  /**
   * @param table string
   * @param where array
   * @param data array
   * 
   * @return boolean
   */
  function delete($table, $where, $data)
  {
    $this->db->update($table, $data, $where);
  }

  /**
   * ===========================================================
   * NUMERIC METHODS
   * ===========================================================
   * 
   * e.g:
   * function total_user()
   * function total_group()
   */

  /**
   * @return num_rows
   */
  function total_user()
  {
    return $this->db->get_where('users', array('has_delete !=' => 1))->num_rows();
  }

  /**
   * @return num_rows
   */
  function total_group()
  {
    return $this->db->get_where('groups', array('has_delete !=' => 1))->num_rows();
  }

  /**
   * ===========================================================
   * CHART METHODS
   * ===========================================================
   * 
   * e.g:
   * function chart_of_bar($table, $date, $year, $start_date = null, $end_date = null)
   * function chart_of_pie($table, $category, $count, $date, $year, $group_by, $start_date = null, $end_date = null)
   * function generate_data($table, $date, $start_date, $end_date, $condition, $table_join = null, $idq = null, $idq_fk = null, $order_by = null, $join = false)
   */

  /**
   * @param table string
   * @param date string
   * @param year string
   * @param start_date string
   * @param end_date string
   * 
   * @return result_array
   */
  function chart_of_bar($table, $date, $year, $start_date = null, $end_date = null)
  {
    $this->db->select('MONTH('. $date .') AS months');
    $this->db->select('COUNT(*) AS count');
    $this->db->from($table);
    $this->db->like('YEAR('. $date .')', $year);
    if ($start_date && $end_date) {
      $this->db->where('DATE('. $date .') >=', $start_date);
      $this->db->where('DATE('. $date .') <=', $end_date);
    }
    $this->db->where('has_delete !=', 1);
    $this->db->group_by('MONTH('. $date .')');

    $query = $this->db->get();
    return $query->result_array();
  }

  /**
   * @param table string
   * @param category string
   * @param count string
   * @param date string
   * @param year string
   * @param group_by string
   * @param start_date string
   * @param end_date string
   * 
   * @return result_array
   */
  function chart_of_pie($table, $category, $count, $date, $year, $group_by, $start_date = null, $end_date = null)
  {
    $this->db->select('DISTINCT('. $category .') AS category');
    $this->db->select('COUNT('. $count .') AS count');
    $this->db->from($table);
    $this->db->like('YEAR('. $date .')', $year);
    if ($start_date && $end_date) {
      $this->db->where('DATE('. $date .') >=', $start_date);
      $this->db->where('DATE('. $date .') <=', $end_date);
    }
    $this->db->where('has_delete !=', 1);
    $this->db->group_by($group_by);

    $query = $this->db->get();
    return $query->result_array();
  }

  /**
   * @param table string
   * @param date string (null)
   * @param start_date string (null)
   * @param end_date string (null)
   * @param condition array (null)
   * @param table_join string (null)
   * @param idq string (null)
   * @param idq_fk string (null)
   * @param order_by string (null)
   * @param join boolean (false)
   * 
   * @return result_array
   */
  function generate_data($table, $date = null, $start_date = null, $end_date = null, $condition = null, $table_join = null, $idq = null, $idq_fk = null, $order_by = null, $join = false)
  {
    $this->db->from($table);
    // Check if variable join is TRUE or FALSE
    if (!$join) {
      $this->db->where('has_delete !=', 1);
    } else {
      $this->db->join($table_join, $table_join . '.' . $idq_fk . '=' . $table . '.' . $idq);
      $this->db->where($table .'.has_delete !=', 1);
    }
    if ($date && $start_date && $end_date) {
      $this->db->where('DATE('. $date .') >=', $start_date);
      $this->db->where('DATE('. $date .') <=', $end_date);
    }
    if ($condition) {
      $this->db->where($condition);
    }
    $this->db->order_by($order_by, 'asc');

    $query = $this->db->get();
    return $query->result_array();
  }
}

/* End of file Core_model.php */
