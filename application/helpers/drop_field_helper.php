<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Fungsi ini digunakan untuk mendapatkan data dari database dan membuatnya
 * sebagai pilihan untuk menu dropdown
 */
function getDropDownList($table, $columns)
{
  $CI = &get_instance();

  $query = $CI->db->select($columns)->from($table)->where('has_delete != 1')->get();

  if ($query->num_rows() >= 1) {
    $options1 = ['' => '-- Select --'];
    $options2 = array_column($query->result_array(), $columns[1], $columns[0]);
    $options = $options1 + $options2;
    return $options;
  }

  return $options = ['' => '-- Select --'];
}

/**
 * Fungsi ini dipakai untuk menampilkan error yang berkaitan dengan input file
 * mengenai jenis file atau ukuran maksimal file yang akan diupload
 */
function fileFormError($field, $prefix = '', $suffix = '')
{
  $CI = &get_instance();

  $error_field = $CI->form_validation->error_array();

  if (!empty($error_field[$field])) {
    return $prefix . $error_field[$field] . $suffix;
  }

  return '';
}
