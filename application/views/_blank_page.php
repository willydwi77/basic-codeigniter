<?php $this->load->view('layout/main/meta') ?>

<div class="wrapper">
  <?php $this->load->view('layout/main/header') ?>
  <?php $this->load->view('layout/main/sidebar') ?>

  <!-- Content Wrapper -->
  <div class="content-wrapper">
    <!-- Content Header -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"> TITLE PAGE </h1>
          </div><!-- /.col -->
          <!-- Breadcrumb -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url('/dashboard') ?>">Link One</a></li>
              <li class="breadcrumb-item active">Link Two</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div><!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
					<div class="col-lg-12">
            <!-- IonAuth Message -->
						<div id="infoMessage"><?php echo $message;?></div>
            <!-- Card -->
						<div class="card">
							<div class="card-header">
								<h3 class="card-title">CARD TITLE</h3>
							</div><!-- /.card-header -->
              <!-- Card Body -->
							<div class="card-body">

                <!-- Put Content Here -->

              </div><!-- /.card-body -->
						</div><!-- /.card -->
					</div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section><!-- /.content -->
	</div><!-- /.content-wrapper -->

  <?php $this->load->view('layout/main/footer') ?>
</div><!-- ./wrapper -->

<?php $this->load->view('layout/main/js') ?>
</body>
</html>