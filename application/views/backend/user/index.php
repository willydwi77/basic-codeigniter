<!-- Content Wrapper -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Users</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard') ?>">Home</a></li>
						<li class="breadcrumb-item active">Users</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
      <!-- Default box -->
			<div class="row">
				<div class="col-lg-12">
					<div class="card card-primary card-outline">
						<div class="card-header">
							<h3 class="card-title">Table User</h3>

							<div class="card-tools">
								<button type="button" class="btn btn-sm btn-success" onclick="add_user()" title="Tambah Data">
									<i class="fas fa-plus"></i> Tambah</button>
								<button type="button" class="btn btn-sm btn-default" onclick="reload_table()" title="Reload">
									<i class="fas fa-sync"></i></button>
							</div>
						</div>
						<div class="card-body">
							<table id="datatable" class="table table-bordered table-striped display nowrap" style="width:100%">
								<thead>
									<tr>
										<th>No</th>
										<th>Name</th>
										<th>Email</th>
										<th>Username</th>
										<th>Groups</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								<tbody>
								<tfoot>
									<tr>
										<th>No</th>
										<th>Name</th>
										<th>Email</th>
										<th>Username</th>
										<th>Groups</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</tfoot>
							</table>
						</div><!-- /.card-body -->
					</div><!-- /.card -->
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
	var table, save_method;
												
	$(document).ready(function () {
		table = $('#datatable').DataTable({
			"processing": true,
			"serverSide": true,
			"responsive": true,
			"order": [],
			"ajax": {
				"url": "<?php echo site_url('user/ajax_list') ?>",
				"type": "POST",
			},
			"columnDefs": [{
				"targets": [0],
				"orderable": false,
			}, {
				"targets": [-1],
				"orderable": false,
			}],
			"rowReorder": {
				"selector": 'td:nth-child(2)'
			},
		});

		// remove class has-error
		$("input").change(function() {
			$(this).parent().parent().removeClass('has-error');
			$(this).next().empty();
		});
		$("textarea").change(function() {
			$(this).parent().parent().removeClass('has-error');
			$(this).next().empty();
		});
		$("select").change(function() {
			$(this).parent().parent().removeClass('has-error');
			$(this).next().empty();
		});
	});

	function reload_table() {
    table.ajax.reload(null, false);
  }

	function add_user() {
		save_method = 'add';
		$('#form')[0].reset();
		$('.form-group').removeClass('has-error');
		$('.help-block').empty();
		$('#modal-form').modal('show');
		$('.modal-title').text('Tambah data');
		$('#btnSave').text('Simpan');
		$('#btnSave').attr('disabled', false);
		$('#identity').attr('disabled', false);
		$('[name="groups"]').attr('disabled', false);
	}

	function edit_user(id) {
		save_method = 'update';
		$('#form')[0].reset();
		$('.form-group').removeClass('has-error');
		$('.help-block').empty();
		$('#btnSave').text('Ubah');
		$('#btnSave').attr('disabled', false);
		$('#identity').attr('disabled', false);
		$('[name="groups"]').attr('disabled', false);

		$.ajax({
			type: "GET",
			url: "<?php echo site_url('user/ajax_edit') ?>/" + id,
			dataType: "JSON",
			success: function (data) {
				$('[name="id"]').val(data.user.id);
				$('[name="first_name"]').val(data.user.first_name);
				$('[name="last_name"]').val(data.user.last_name);
				$('[name="phone"]').val(data.user.phone);
				$('[name="company"]').val(data.user.company);
				$('[name="identity"]').val(data.user.username);
				$('[name="groups"]').val(data.currentGroups.id);
				$('[name="email"]').val(data.user.email);
				$('#modal-form').modal('show');
				$('.modal-title').text('Edit data');

				if (data.user.username === 'administrator') {
					$('#identity').attr('disabled', true);
					$('[name="groups"]').attr('disabled', true);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert(textStatus);
			}
		});
	}

	function save() {
		$('#btnSave').text('Menyimpan ...');
		$('#btnSave').attr('disabled', true);

		var url;

		if (save_method == 'add') {
			url = "<?php echo site_url('user/ajax_add') ?>"
		} else {
			url = "<?php echo site_url('user/ajax_update') ?>"
		}

		$.ajax({
			type: "POST",
			url: url,
			data: $('#form').serialize(),
			dataType: "JSON",
			success: function (data) {
				if (data.status) {
          $('#modal-form').modal('hide');
          reload_table();

					Swal.fire({
						icon: 'success',
						title: 'Sukses',
						text: 'Data telah tersimpan'
					});
        } else {
					$('.message').html(data.message).show();
				}

				$('#btnSave').text('Simpan');
				$('#btnSave').attr('disabled', false);
				$('#identity').attr('disabled', false);
				$('[name="groups"]').attr('disabled', false);
			},
			error: function (jqXHR, textStatus, errorThrown) {
				$('#btnSave').text('Simpan');
				$('#btnSave').attr('disabled', false);
				$('#identity').attr('disabled', false);
				$('[name="groups"]').attr('disabled', false);
			}
		});
	}

	function delete_user(id) {
		Swal.fire({
			title: 'Hapus data?',
			text: "Data akan dihapus dari sistem",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Iya'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('user/ajax_delete/') ?>" + id,
					dataType: "JSON",
					success: function (data) {
						$('#modal-form').modal('hide');
						reload_table();

						Swal.fire(
							'Terhapus',
							'Data yang dimaksud sudah dihapus',
							'success'
						)
					},
					error: function (jqXHR, textStatus, errorThrown) {
						alert(textStatus);
					}
				});		
			}
		})
	}

	function deactivate(id) {
		console.log(id)
		Swal.fire({
			title: 'Non-aktifkan user?',
			text: "User akan di non-aktifkan",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Iya'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('user/ajax_deactivated/') ?>" + id,
					dataType: "JSON",
					success: function (data) {
						$('#modal-form').modal('hide');
						reload_table();

						Swal.fire(
							'Non-aktif',
							'User sudah di non-aktifkan',
							'success'
						)
					},
					error: function (jqXHR, textStatus, errorThrown) {
						alert(textStatus);
					}
				});		
			}
		})
	}

	function activate(id) {
		Swal.fire({
			title: 'Aktifkan user?',
			text: "User akan diaktifkan",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Iya'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('user/ajax_activated/') ?>" + id,
					dataType: "JSON",
					success: function (data) {
						$('#modal-form').modal('hide');
						reload_table();

						Swal.fire(
							'Aktif',
							'User sudah diaktifkan',
							'success'
						)
					},
					error: function (jqXHR, textStatus, errorThrown) {
						alert(textStatus);
					}
				});		
			}
		})
	}
</script>

<!-- Modal -->
<div class="modal fade" id="modal-form">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body form">
				<div class="message"></div>
        <form action="#" id="form" class="form-horizontal">
					<!-- id -->
					<input type="hidden" value="" name="id" />
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="first_name" class="col-form-label">Nama Depan</label>
								<span class="help-block"></span>
								<input type="text" class="form-control" id="first_name" name="first_name" placeholder="Nama Depan"  tabindex="1">
							</div>
							<div class="form-group">
								<label for="phone" class="col-form-label">No Telepon</label>
								<span class="help-block"></span>
								<input type="text" class="form-control" id="phone" name="phone" placeholder="No Telepon"  tabindex="3">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="last_name" class="col-form-label">Nama Belakang</label>
								<span class="help-block"></span>
								<input type="text" class="form-control" id="last_name" name="last_name" placeholder="Nama Belakang"  tabindex="2">
							</div>
							<div class="form-group">
								<label for="company" class="col-form-label">Persuhaan</label>
								<span class="help-block"></span>
								<input type="text" class="form-control" id="company" name="company" placeholder="Perusahaan"  tabindex="4">
							</div>
						</div>
					</div>
					<br />
					<h5>Autentikasi <i class="fas fa-lock"></i></h5>
					<hr />
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="identity" class="col-form-label">Username</label>
								<span class="help-block"></span>
								<div class="input-group mb-3">
									<input type="text" class="form-control" id="identity" name="identity" placeholder="Username" tabindex="5">
									<div class="input-group-append">
										<span class="input-group-text"><i class="fas fa-user"></i></span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="identity" class="col-form-label">Password</label>
								<span class="help-block"></span>
								<div class="input-group mb-3">
									<input type="password" class="form-control" id="password" name="password" placeholder="Password" tabindex="7">
									<div class="input-group-append">
										<span class="input-group-text"><i class="fas fa-lock"></i></span>
									</div>
								</div>
							</div>
							<?php if ($this->ion_auth->is_admin()) { ?>
							<div class="form-group">
								<label for="groups" class="col-form-label">Roles</label>
								<span class="help-block"></span>
								<?php
									echo form_dropdown('groups', getDropDownList('groups', ['id', 'name']), 'id="groups"', [
										'class' => 'form-control',
										'tabindex' => 9
									]);
								?>
							</div>
							<?php } ?>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="email" class="col-form-label">Email</label>
								<span class="help-block"></span>
								<div class="input-group mb-3">
									<input type="text" class="form-control" id="email" name="email" placeholder="Email"  tabindex="6">
									<div class="input-group-append">
										<span class="input-group-text"><i class="fas fa-envelope"></i></span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="password_confirm" class="col-form-label">Konfirmasi Password</label>
								<span class="help-block"></span>
								<div class="input-group mb-3">
									<input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Konfirmasi Password" tabindex="8">
									<div class="input-group-append">
										<span class="input-group-text"><i class="fas fa-lock"></i></span>
									</div>
								</div>
							</div>
						</div>
					</div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary" onclick="save()" id="btnSave"></button>
      </div>
    </div>
  </div>
</div>