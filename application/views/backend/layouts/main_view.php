<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/') ?>datatables-bs4/css/dataTables.bootstrap4.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/') ?>datatables-rowreorder/css/rowReorder.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/') ?>datatables-responsive/css/responsive.bootstrap4.min.css">
  <!-- DateRangePicker -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/') ?>daterangepicker/daterangepicker.css" />
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/') ?>overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/') ?>fontawesome-free/css/all.min.css">
	<!-- SweetAlert 2 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/') ?>sweetalert2/sweetalert2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/') ?>adminlte/css/adminlte.min.css">
  <!-- IonIcons -->
  <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

	<!-- jQuery -->
	<script src="<?php echo base_url('assets/plugins/') ?>jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="<?php echo base_url('assets/plugins/') ?>bootstrap/js/bootstrap.min.js"></script>
	<!-- DataTable -->
	<script src="<?php echo base_url('assets/plugins/') ?>datatables/jquery.dataTables.js"></script>
	<script src="<?php echo base_url('assets/plugins/') ?>datatables-bs4/js/dataTables.bootstrap4.js"></script>
	<script src="<?php echo base_url('assets/plugins/') ?>datatables-rowreorder/js/rowReorder.bootstrap4.min.js"></script>
	<script src="<?php echo base_url('assets/plugins/') ?>datatables-responsive/js/responsive.bootstrap4.min.js"></script>
	<!-- MomentJS -->
	<script src="<?php echo base_url('assets/plugins/') ?>moment/moment.min.js"></script>
	<script src="<?php echo base_url('assets/plugins/') ?>moment/moment-with-locales.min.js"></script>
	<!-- DateRangePicker -->
	<script src="<?php echo base_url('assets/plugins/') ?>daterangepicker/daterangepicker.js"></script>
	<!-- ChartJS -->
	<script src="<?php echo base_url('assets/plugins/') ?>chart.js/Chart.min.js"></script>
	<!-- overlayScrollbars -->
	<script src="<?php echo base_url('assets/plugins/') ?>overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
	<!-- SweetAlert 2 -->
	<script src="<?php echo base_url('assets/plugins/') ?>sweetalert2/sweetalert2.min.js"></script>
	<!-- AdminLTE -->
	<script src="<?php echo base_url('assets/plugins/') ?>adminlte/js/adminlte.min.js"></script>
	<!-- Custom DataTable -->
	<script>
		$(function () {
			$('#table').DataTable()
		})
	</script>
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<!-- Wrapper -->
<div class="wrapper">
	<?php $this->load->view('backend/layouts/_main/_navbar') ?>
	<?php $this->load->view('backend/layouts/_main/_sidebar') ?>

	<!-- load content is here -->
	<?php echo $content; ?>
	
	<!-- Footer -->
	<footer class="main-footer">
		<?php echo $copyright ?>
		<div class="float-right d-none d-sm-inline-block">
			<?php echo $version ?>
		</div>
	</footer>
</div><!-- ./wrapper -->
</body>
</html>