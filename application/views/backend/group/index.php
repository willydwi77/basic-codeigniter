<!-- Content Wrapper -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Groups</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard') ?>">Home</a></li>
            <li class="breadcrumb-item active">Groups</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Default box -->
      <div class="row">
        <div class="col-lg-12">
          <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">Group List</h3>

              <div class="card-tools">
								<button type="button" class="btn btn-sm btn-success" onclick="add_group()" title="Tambah Data">
									<i class="fas fa-plus"></i> Tambah</button>
								<button type="button" class="btn btn-sm btn-default" onclick="reload_table()" title="Reload">
									<i class="fas fa-sync"></i></button>
							</div>
            </div>
            <div class="card-body">
              <table id="datatable" class="table table-bordered table-striped display nowrap" style="width:100%">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Group</th>
                    <th>Deskripsi</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                <tbody>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Nama Group</th>
                    <th>Deskripsi</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
              </table>
            </div><!-- /.card-body -->
          </div><!-- /.card -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
  var table, save_method;

  $(document).ready(function () {
    table = $('#datatable').DataTable({
      "processing": true,
      "serverSide": true,
      "responsive": true,
      "order": [],
      "ajax": {
        "url": "<?php echo site_url('group/ajax_list') ?>",
        "type": "POST",
      },
      "columnDefs": [{
        "targets": [-1],
        "orderable": false,
      }],
      rowReorder: {
        selector: 'td:nth-child(2)'
      },
    });

		// remove class has-error
    $("input").change(function() {
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
    });
    $("textarea").change(function() {
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
    });
    $("select").change(function() {
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
    });
  });

  function reload_table() {
    table.ajax.reload(null, false);
  }

  function add_group() {
		save_method = 'add';
		$('#form')[0].reset();
		$('.form-group').removeClass('has-error');
		$('.help-block').empty();
		$('#modal-form').modal('show');
		$('.modal-title').text('Tambah data');
		$('#btnSave').text('Simpan');
		$('#btnSave').attr('disabled', false);
	}

  function edit_group(id) {
		save_method = 'update';
		$('#form')[0].reset();
		$('.form-group').removeClass('has-error');
		$('.help-block').empty();
    $('[name="group_name"]').attr('disabled', false);
		$('#btnSave').text('Ubah');
		$('#btnSave').attr('disabled', false);

		$.ajax({
			type: "GET",
			url: "<?php echo site_url('group/ajax_edit') ?>/" + id,
			dataType: "JSON",
			success: function (data) {
				$('[name="id"]').val(data.id);
				$('[name="group_name"]').val(data.name);
				$('[name="description"]').val(data.description);

        // cek jika group adalah admin maka readonly
        if (data.name === 'admin') {
          $('[name="group_name"]').val(data.name).attr('disabled', true);
        } else {
          $('[name="group_name"]').val(data.name);
        }

				$('#modal-form').modal('show');
				$('.modal-title').text('Edit data');
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert(textStatus);
			}
		});
	}

  function save() {
		$('#btnSave').text('Menyimpan ...');
		$('#btnSave').attr('disabled', true);

		var url;

		if (save_method == 'add') {
			url = "<?php echo site_url('group/ajax_add') ?>"
		} else {
			url = "<?php echo site_url('group/ajax_update') ?>"
		}

		$.ajax({
			type: "POST",
			url: url,
			data: $('#form').serialize(),
			dataType: "JSON",
			success: function (data) {
				if (data.status) {
          $('#modal-form').modal('hide');
          reload_table();

					Swal.fire({
						icon: 'success',
						title: 'Sukses',
						text: 'Data telah tersimpan'
					});
        } else {
					$('.message').html(data.message).show();
				}

        $('[name="group_name"]').attr('disabled', false);
				$('#btnSave').text('Simpan');
				$('#btnSave').attr('disabled', false);
			},
			error: function (jqXHR, textStatus, errorThrown) {
				console.log(errorThrown);

        $('[name="group_name"]').attr('disabled', false);
				$('#btnSave').text('Simpan');
				$('#btnSave').attr('disabled', false);
			}
		});
	}
</script>

<!-- Modal -->
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body form">
				<div class="message"></div>
        <form action="#" id="form" class="form-horizontal">
					<!-- id -->
					<input type="hidden" value="" name="id" />
          <div class="form-group">
            <label for="group_name" class="col-form-label">Nama Group *)</label>
						<span class="help-block"></span>
            <input type="text" class="form-control" id="group_name" name="group_name" placeholder="Nama Grup" tabindex="1">
          </div>
					<div class="form-group">
            <label for="description" class="col-form-label">Deskripsi</label>
						<span class="help-block"></span>
            <input type="text" class="form-control" id="description" name="description" placeholder="Deskripsi" tabindex="2">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary" onclick="save()" id="btnSave"></button>
      </div>
    </div>
  </div>
</div>