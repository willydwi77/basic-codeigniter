<div class="login-box">
  <div class="login-logo">
    <h1><?php echo lang('login_heading');?></h1>
  </div><!-- /.login-logo -->
  <div class="card card-primary card-outline">
    <div class="card-body login-card-body">
      <p class="login-box-msg"><?php echo lang('login_subheading');?></p>

      <!-- IonAuth Message -->
      <div id="infoMessage"><?php echo $message;?></div>

      <?php echo form_open("auth/login");?>
        <div class="input-group mb-3">
          <?php echo form_input($identity);?>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <?php echo form_input($password);?>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8"></div><!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block"><?php echo lang('login_submit_btn') ?></button>
          </div><!-- /.col -->
        </div>
      <?php echo form_close();?>
    </div><!-- /.login-card-body -->
  </div>
</div><!-- /.login-box -->