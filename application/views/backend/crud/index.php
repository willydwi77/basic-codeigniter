<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1>Simple CRUD Operation</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard') ?>">Home</a></li>
						<li class="breadcrumb-item active">CRUD Operation</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<!-- Default box -->
					<div class="card card-primary card-outline">
						<div class="card-header">
							<h3 class="card-title">CRUD Operation</h3>

							<div class="card-tools">
								<button type="button" class="btn btn-sm btn-success" onclick="add_person()" title="Tambah Data">
									<i class="fas fa-plus"></i> Tambah</button>
								<button type="button" class="btn btn-sm btn-default" onclick="reload_table()" title="Reload">
									<i class="fas fa-sync"></i></button>
							</div>
						</div>
						<div class="card-body">
							<table id="datatable" class="table table-bordered table-striped display nowrap" style="width:100%">
								<thead>
                  <tr>
										<th>No</th>
                    <th>Nama Lengkap</th>
                    <th>Alamat</th>
                    <th>No Telepon</th>
                    <th>Actions</th>
                  </tr>
								</thead>
								<tbody>
								</tbody>
								<tfoot>
                  <tr>
										<th>No</th>
                    <th>Nama Lengkap</th>
                    <th>Alamat</th>
                    <th>No Telepon</th>
                    <th>Actions</th>
                  </tr>
								</tfoot>
							</table>
						</div>
						<!-- /.card-body -->
					</div>
					<!-- /.card -->
				</div>
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
	var table, save_method;
  
  $(document).ready(function () {
    table = $('#datatable').DataTable({
      "processing": true,
      "serverSide": true,
      "responsive": true,
      "order": [],
      "ajax": {
        "url": "<?php echo site_url('crud/ajax_list') ?>",
        "type": "POST",
      },
      "columnDefs": [{
        "targets": [-1],
        "orderable": false,
      }],
    });

		// remove class has-error
    $("input").change(function() {
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
    });
    $("textarea").change(function() {
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
    });
    $("select").change(function() {
      $(this).parent().parent().removeClass('has-error');
      $(this).next().empty();
    });
  });

  function reload_table() {
    table.ajax.reload(null, false);
  }

	function add_person() {
		save_method = 'add';
		$('#form')[0].reset();
		$('.form-group').removeClass('has-error');
		$('.help-block').empty();
		$('#modal-form').modal('show');
		$('.modal-title').text('Tambah data');
		$('#btnSave').text('Simpan');
		$('#btnSave').attr('disabled', false);
	}

	function edit_person(id) {
		save_method = 'update';
		$('#form')[0].reset();
		$('.form-group').removeClass('has-error');
		$('.help-block').empty();
		$('#btnSave').text('Ubah');
		$('#btnSave').attr('disabled', false);

		$.ajax({
			type: "GET",
			url: "<?php echo site_url('crud/ajax_edit') ?>/" + id,
			dataType: "JSON",
			success: function (data) {
				$('[name="id_person"]').val(data.id_person);
				$('[name="nama_lengkap"]').val(data.nama_lengkap);
				$('[name="alamat"]').val(data.alamat);
				$('[name="no_telepon"]').val(data.no_telepon);
				$('#modal-form').modal('show');
				$('.modal-title').text('Edit data');
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert(textStatus);
			}
		});
	}

	function save() {
		$('#btnSave').text('Menyimpan ...');
		$('#btnSave').attr('disabled', true);

		var url;

		if (save_method == 'add') {
			url = "<?php echo site_url('crud/ajax_add') ?>"
		} else {
			url = "<?php echo site_url('crud/ajax_update') ?>"
		}

		$.ajax({
			type: "POST",
			url: url,
			data: $('#form').serialize(),
			dataType: "JSON",
			success: function (data) {
				if (data.status) {
          $('#modal-form').modal('hide');
          reload_table();

					Swal.fire({
						icon: 'success',
						title: 'Sukses',
						text: 'Data telah tersimpan'
					});
        } else {
					$('.message').html(data.message).show();
				}

				$('#btnSave').text('Simpan');
				$('#btnSave').attr('disabled', false);
			},
			error: function (jqXHR, textStatus, errorThrown) {
				console.log(errorThrown);
				$('#btnSave').text('Simpan');
				$('#btnSave').attr('disabled', false);
			}
		});
	}

	function delete_person(id) {
		Swal.fire({
			title: 'Hapus data?',
			text: "Data akan dihapus dari sistem",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Iya'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('crud/ajax_delete/') ?>" + id,
					dataType: "JSON",
					success: function (data) {
						$('#modal-form').modal('hide');
						reload_table();

						Swal.fire(
							'Terhapus',
							'Data yang dimaksud sudah dihapus',
							'success'
						)
					},
					error: function (jqXHR, textStatus, errorThrown) {
						alert(textStatus);
					}
				});		
			}
		})
	}
</script>

<!-- Modal -->
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body form">
				<div class="message"></div>
        <form action="#" id="form" class="form-horizontal">
					<!-- id -->
					<input type="hidden" value="" name="id_person" />
          <div class="form-group">
            <label for="nama_lengkap" class="col-form-label">Nama Lengkap *)</label>
						<span class="help-block"></span>
            <input type="text" class="form-control" id="nama_lengkap" name="nama_lengkap">
          </div>
          <div class="form-group">
            <label for="alamat" class="col-form-label">Alamat</label>
						<span class="help-block"></span>
            <textarea class="form-control" id="alamat" name="alamat"></textarea>
          </div>
					<div class="form-group">
            <label for="no_telepon" class="col-form-label">No Telepon *)</label>
						<span class="help-block"></span>
            <input type="text" class="form-control" id="no_telepon" name="no_telepon">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary" onclick="save()" id="btnSave"></button>
      </div>
    </div>
  </div>
</div>