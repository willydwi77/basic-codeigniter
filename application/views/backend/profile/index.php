<div class="content-wrapper" style="min-height: 3203px;">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Users Profile</h1>
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard') ?>">Home</a></li>
						<li class="breadcrumb-item active">Users Profile</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12">
					<!-- Default box -->
					<div class="card card-primary card-outline">
						<div class="card-header">
							<h3 class="card-title">Title</h3>
						</div>
						<div class="card-body">
							<div class="message"></div>
								<form action="#" id="form" class="form-horizontal">
									<!-- id -->
									<input type="hidden" value="" name="id" />
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="first_name" class="col-form-label">Nama Depan</label>
												<span class="help-block"></span>
												<input type="text" class="form-control" id="first_name" name="first_name" tabindex="1">
											</div>
											<div class="form-group">
												<label for="phone" class="col-form-label">No Telepon</label>
												<span class="help-block"></span>
												<input type="text" class="form-control" id="phone" name="phone" tabindex="3">
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="last_name" class="col-form-label">Nama Belakang</label>
												<span class="help-block"></span>
												<input type="text" class="form-control" id="last_name" name="last_name" tabindex="2">
											</div>
											<div class="form-group">
												<label for="company" class="col-form-label">Persuhaan</label>
												<span class="help-block"></span>
												<input type="text" class="form-control" id="company" name="company" tabindex="4">
											</div>
										</div>
									</div>
									<br />
									<h5>Autentikasi</h5>
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="identity" class="col-form-label">Username</label>
												<span class="help-block"></span>
												<input type="text" class="form-control" id="identity" name="identity" tabindex="5">
											</div>
											<div class="form-group">
												<label for="password" class="col-form-label">Password</label>
												<span class="help-block"></span>
												<input type="password" class="form-control" id="password" name="password" tabindex="7">
											</div>
											<?php if ($this->ion_auth->is_admin()) { ?>
											<div class="form-group">
												<label for="groups" class="col-form-label">Roles</label>
												<span class="help-block"></span>
												<?php
													echo form_dropdown('groups', getDropDownList('groups', ['id', 'name']), 'id="groups"', ['class' => 'form-control', 'tabindex' => 9]);
												?>
											</div>
											<?php } ?>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="email" class="col-form-label">Email</label>
												<span class="help-block"></span>
												<input type="text" class="form-control" id="email" name="email" tabindex="6">
											</div>
											<div class="form-group">
												<label for="password_confirm" class="col-form-label">Konfirmasi Password</label>
												<span class="help-block"></span>
												<input type="password" class="form-control" id="password_confirm" name="password_confirm" tabindex="8">
											</div>
										</div>
									</div>
								</form>
							</div>
						<!-- /.card-body -->
						<div class="card-footer">
							<button type="button" class="btn btn-secondary" onclick="window.history.back()">Batal</button>
							<button type="button" class="btn btn-primary" onclick="save()" id="btnSave"></button>
						</div>
						<!-- /.card-footer-->
					</div>
					<!-- /.card -->
				</div>
			</div>
		</div>
	</section>
	<!-- /.content -->
</div>

<script>
	$(document).ready(function () {
		$id = "<?php echo $current_user->id ?>"
		$.ajax({
			type: "GET",
			url: "<?php echo site_url('profile/ajax_edit') ?>/" + $id,
			dataType: "JSON",
			success: function (data) {
				$('[name="id"]').val(data.user.id);
				$('[name="first_name"]').val(data.user.first_name);
				$('[name="last_name"]').val(data.user.last_name);
				$('[name="phone"]').val(data.user.phone);
				$('[name="company"]').val(data.user.company);
				$('[name="identity"]').val(data.user.username);
				$('[name="groups"]').val(data.currentGroups.id);
				$('[name="email"]').val(data.user.email);
				$('#btnSave').text('Save');

				if (data.user.username === 'administrator') {
					$('#identity').attr('disabled', true);
					$('[name="groups"]').attr('disabled', true);
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				alert(textStatus);
			}
		});
	})
</script>