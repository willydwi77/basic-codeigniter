<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends Users_Controller {
  
  var $join = false;
	var $table = 'groups';
  var $idq = 'id';
  var $column_order = array('name', 'description', null);
  var $column_search = array('name');
	var $order = array('id' => 'desc');

  public function __construct() {
    parent::__construct();
    $this->lang->load('auth');
  }

  public function index()
  {
    if (!$this->ion_auth->logged_in()) {
      // redirect them to the errors page because they must be an reportsistrator to view this
      $this->session->set_flashdata('message', 'Please login first <a href="'. base_url('auth/login') .'">Login</a>.');
      redirect('auth/errors', 'refresh');
    }
  }

  public function ajax_list() {
    $list = $this->core_model->get_datatables();
    $data = array();
    $no = $_POST['start'];

    foreach ($list as $items) {
      $no++;
			$row = array();
			$row[] = $no;
      $row[] = $items->name;
      $row[] = $items->description;
      $row[] = '
        <a class="btn btn-sm btn-warning" href="javascript:void(0)" title="Edit" onclick="edit_group(' . "'" . $items->id . "'" . ')">
        	<i class="fa fa-edit"></i></a>
      ';

      $data[] = $row;
    }

    $output = array(
      'draw' => $_POST['draw'],
      'recordsTotal' => $this->core_model->count_all(),
      'recordsFiltered' => $this->core_model->count_filtered(),
      'data' => $data,
    );

    echo json_encode($output);
  }

  function ajax_edit($id) {
    $data = $this->ion_auth->group($id)->row();
    echo json_encode($data);
  }
  
  function ajax_add() {
    // validation
    $this->form_validation->set_rules('group_name', $this->lang->line('create_group_validation_name_label'), 'trim|required|alpha_dash');
  
    if ($this->form_validation->run()) {
      $insert = $this->ion_auth->create_group($this->input->post('group_name'), $this->input->post('description'));

      echo json_encode(array(
        'status' => true,
        'message', $this->ion_auth->messages()
      ));
    } else {
      $error = '
      <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h5><i class="icon fas fa-exclamation-triangle"></i> Alert!</h5>
          '. validation_errors() .'
      </div>
    ';

      echo json_encode(array('status' => false, 'message' => $error));
    }
  }

  public function ajax_update() {
    // validation
    $this->form_validation->set_rules('group_name', $this->lang->line('edit_group_validation_name_label'), 'trim|required|alpha_dash');

    if ($this->form_validation->run()) {
      $id = html_escape($this->input->post('id'));

      $this->ion_auth->update_group($id, $this->input->post('group_name'), array(
        'description' => $this->input->post('description')
      ));

      echo json_encode(array('status' => true));			
    }
  }
}

/* End of file Group.php */