<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Users_Controller {

	public function __construct() {
		parent::__construct();
		$this->config->load('ion_auth', TRUE);
		$this->load->model('core_model');
	}

	public function index() {
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the errors page because they must be an reportsistrator to view this
			$this->session->set_flashdata('message', 'Please login first <a href="'. base_url('auth/login') .'">Login</a>.');
			redirect('auth/errors', 'refresh');
		}
	}

	public function dashboard() {
		$this->render('backend/dashboard/index');
	}

	public function crud() {
		$this->render('backend/crud/index');
	}

	public function user() {
		$this->render('backend/user/index');
	}

	public function group() {
		$this->render('backend/group/index');
	}

	public function profile() {
		$this->render('backend/profile/index');
	}
}
