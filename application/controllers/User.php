<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Users_Controller {
  
  var $join = false;
	var $table = 'users';
  var $idq = 'id';
  var $column_order = array('email', 'username', 'active', null);
  var $column_search = array('email', 'username');
	var $order = array('first_name' => 'asc');

  public function __construct() {
    parent::__construct();
    $this->lang->load('auth');
  }
  
  public function index() {
    if (!$this->ion_auth->logged_in()) {
      // redirect them to the errors page because they must be an reportsistrator to view this
      $this->session->set_flashdata('message', 'Please login first <a href="'. base_url('auth/login') .'">Login</a>.');
      redirect('auth/errors', 'refresh');
    }
  }

  public function ajax_list() {
    $list = $this->core_model->get_datatables();
    $data = array();
    $no = $_POST['start'];

    foreach ($list as $k => $user) {
      $list[$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
    }

    foreach ($list as $items) {
      foreach ($items->groups as $k => $group) {
        $user_group[$k] = $group->name;
      }

      if ($items->username === 'administrator' && $items->active) {
        $status = lang('index_active_link');
      } elseif ($items->active) {
        $status = '<a href="javascript:void(0)" onclick="deactivate(' . "'" . $items->id . "'" . ')">'. lang('index_active_link') .'</a>';
      } else {
        $status = '<a href="javascript:void(0)" onclick="activate(' . "'" . $items->id . "'" . ')">' . lang('index_inactive_link') . '</a>';
      }

      $no++;
			$row = array();
			$row[] = $no;
      $row[] = $items->first_name .' '. $items->last_name;
      $row[] = $items->email;
      $row[] = $items->username;
      $row[] = $user_group;
      $row[] = $status;
      $row[] = $items->username === 'administrator' ? '
        <a class="btn btn-sm btn-warning" href="javascript:void(0)" title="Edit" onclick="edit_user(' . "'" . $items->id . "'" . ')">
        	<i class="fa fa-edit"></i></a>
      ' : '
        <a class="btn btn-sm btn-warning" href="javascript:void(0)" title="Edit" onclick="edit_user(' . "'" . $items->id . "'" . ')">
          <i class="fa fa-edit"></i></a>
        <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_user(' . "'" . $items->id . "'" . ')">
          <i class="fa fa-trash"></i></a>
      ';

      $data[] = $row;
    }

    $output = array(
      'draw' => $_POST['draw'],
      'recordsTotal' => $this->core_model->count_all(),
      'recordsFiltered' => $this->core_model->count_filtered(),
      'data' => $data
    );

    echo json_encode($output);
  }

  function ajax_edit($id) {
    $data = array(
      'user' => $this->ion_auth->user($id)->row(),
      'currentGroups' => $this->ion_auth->get_users_groups($id)->row()
    );
    
    echo json_encode($data);
	}

  function ajax_add() {
    $tables = $this->config->item('tables', 'ion_auth');
    // $identity_column = $this->config->item('identity', 'ion_auth');
    // $this->data['identity_column'] = $identity_column;

    // validation
    $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'trim|required');
    $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'trim|required');
    $this->form_validation->set_rules('identity', $this->lang->line('create_user_validation_identity_label'), 'trim|required|is_unique[' . $tables['users'] . '.username]');
    $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'trim|required|valid_email');
    $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'trim');
    $this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'trim');
    $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|matches[password_confirm]');
    $this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');
    
    if ($this->form_validation->run()) {
      $email = strtolower($this->input->post('email'));
      $identity = $this->input->post('identity');
      $password = $this->input->post('password');

      $additional_data = [
        'first_name' => $this->input->post('first_name'),
        'last_name' => $this->input->post('last_name'),
        'company' => $this->input->post('company'),
        'phone' => $this->input->post('phone'),
      ];

      $insert = $this->ion_auth->register($identity, $password, $email, $additional_data);
      echo json_encode(array('status' => true, 'inserted' => $insert));
    } else {
      $error = '
        <div class="alert alert-warning alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h5><i class="icon fas fa-exclamation-triangle"></i> Alert!</h5>
            '. validation_errors() .'
        </div>
      ';
      
      echo json_encode(array('status' => false, 'message' => $error));
    }
  }

  function ajax_update() {
    // validation
    $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'trim|required');
    $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'trim|required');
    $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'trim|required|valid_email');
    $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'trim');
    $this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'trim');
    
    if ($this->input->post('password')) {
      // validation
      $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|matches[password_confirm]');
      $this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');
    }

    if ($this->form_validation->run()) {
      $data = [
        'first_name' => $this->input->post('first_name'),
        'last_name' => $this->input->post('last_name'),
        'username' => $this->input->post('identity'),
        'company' => $this->input->post('company'),
        'phone' => $this->input->post('phone'),
        'email' => strtolower($this->input->post('email'))
      ];

      // update password jika post password terisi
      if ($this->input->post('password')) {
        $data['password'] = $this->input->post('password');
      }

      // hanya mengijinkan admin untuk merubah group user
      if ($this->ion_auth->is_admin()) {
        $id = $this->input->post('id');
        $groupData = $this->input->post('groups');

        if (isset($groupData) && !empty($groupData)) {
          // hapus user group untuk menghindari duplikasi
          $this->ion_auth->remove_from_group('', $id);

          $this->ion_auth->add_to_group($groupData, $id);
        }
      }

      $this->ion_auth->update($this->input->post('id'), $data);

      echo json_encode(array('status' => true));
    } else {
      $error = '
        <div class="alert alert-warning alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h5><i class="icon fas fa-exclamation-triangle"></i> Alert!</h5>
            '. validation_errors() .'
        </div>
      ';
      
      echo json_encode(array('status' => false, 'message' => $error));
    }
  }

  function ajax_delete($id) {
    $date = new DateTime();

    $data = array(
      'has_delete' => 1
    );

    $this->core_model->delete('users', array($this->idq => $id), $data);
    echo json_encode(array('status' => true));
  }
  
  function ajax_activated($id, $code = false) {
    $activation = false;

    if ($code !== false) {
      $activation = $this->ion_auth->activate($id, $code);
    } elseif ($this->ion_auth->is_admin()) {
      $activation = $this->ion_auth->activate($id);
    }

    echo json_encode(array('status' => true));
  }

  function ajax_deactivated($id = null) {
    // $this->authorization();

    $id = (int)$id;

    // validation
    // $this->form_validation->set_rules('id', $this->lang->line('deactivate_validation_user_id_label'), 'required|alpha_numeric');

    $this->ion_auth->deactivate($id);
    echo json_encode(array('status' => true));
  }

  function redirectUser(){
		if ($this->ion_auth->is_admin()){
			redirect('auth', 'refresh');
		}
		redirect('/', 'refresh');
  }
  
  function authorization() {
    if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin()) {
      // redirect them to the home page because they must be an administrator to view this
      show_error('You must be an administrator to view this page.');
    }
  }
}

/* End of file User.php */