<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends Users_Controller {

	public function __construct() {
    parent::__construct();
    $this->lang->load('auth');
	}
	
	function ajax_edit($id) {
    $data = array(
      'user' => $this->ion_auth->user($id)->row(),
      'currentGroups' => $this->ion_auth->get_users_groups($id)->row()
    );
    
    echo json_encode($data);
	}
}

/* End of file Profile.php */
