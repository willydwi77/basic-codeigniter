<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud extends Users_Controller {

	var $join = false;
	var $table = 'person';
  var $idq = 'id_person';
  var $column_order = array('nama_lengkap', 'alamat', 'no_telepon', null);
  var $column_search = array('nama_lengkap');
	var $order = array('nama_lengkap' => 'asc');
	
	function __construct() {
		parent::__construct();
		$this->load->model('core_model');
	}

	function index() {
		if (!$this->ion_auth->logged_in()) {
			// redirect them to the errors page because they must be an reportsistrator to view this
			$this->session->set_flashdata('message', 'Please login first <a href="'. base_url('auth/login') .'">Login</a>.');
			redirect('auth/errors', 'refresh');
		}
	}

	function ajax_list() {
		$list = $this->core_model->get_datatables();
    $data = array();
    $no = $_POST['start'];

    foreach ($list as $items) {
      $no++;
			$row = array();
			$row[] = $no;
      $row[] = $items->nama_lengkap;
      $row[] = $items->alamat;
      $row[] = $items->no_telepon;
      $row[] = '
        <a class="btn btn-sm btn-warning" href="javascript:void(0)" title="Edit" onclick="edit_person(' . "'" . $items->id_person . "'" . ')">
        	<i class="fa fa-edit"></i></a>
        <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_person(' . "'" . $items->id_person . "'" . ')">
        	<i class="fa fa-trash"></i></a>
      ';

      $data[] = $row;
    }

    $output = array(
      'draw' => $_POST['draw'],
      'recordsTotal' => $this->core_model->count_all(),
      'recordsFiltered' => $this->core_model->count_filtered(),
      'data' => $data,
    );

    echo json_encode($output);
	}

	function ajax_edit($id) {
    $data = $this->core_model->get_by_row($this->table, array('id_person' => $id));
    echo json_encode($data);
	}
	
	function ajax_add() {
    // validation
    $this->form_validation->set_rules('nama_lengkap', 'Name lengkap', 'required');
    $this->form_validation->set_rules('alamat', 'Alamat', 'trim');
    $this->form_validation->set_rules('no_telepon', 'No Telepon', 'numeric|required');

    if ($this->form_validation->run()) {
      $date = new DateTime();

      $data = array(
        'nama_lengkap' => html_escape($this->input->post('nama_lengkap')),
        'alamat' => html_escape($this->input->post('alamat')),
        'no_telepon' => html_escape($this->input->post('no_telepon')),
        'created_at' => $date->getTimestamp(),
      );

      $insert = $this->core_model->create($this->table, $data);
      echo json_encode(array('status' => true));
    } else {
      $error = '
        <div class="alert alert-warning alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <h5><i class="icon fas fa-exclamation-triangle"></i> Alert!</h5>
            '. validation_errors() .'
        </div>
      ';
      
      echo json_encode(array('status' => false, 'message' => $error));
    }
	}
	
	function ajax_update() {
    // validation
    $this->form_validation->set_rules('nama_lengkap', 'Name lengkap', 'required');
    $this->form_validation->set_rules('alamat', 'Alamat', 'trim');
    $this->form_validation->set_rules('no_telepon', 'No Telepon', 'numeric|required');

    if ($this->form_validation->run()) {
      $id = html_escape($this->input->post('id_person'));

      $date = new DateTime();

      $data = array(
        'nama_lengkap' => html_escape($this->input->post('nama_lengkap')),
        'alamat' => html_escape($this->input->post('alamat')),
        'no_telepon' => html_escape($this->input->post('no_telepon')),
        'updated_at' => $date->getTimestamp(),
      );

      $this->core_model->update($this->table, array($this->idq => $id), $data);
      echo json_encode(array('status' => true));
    }
	}
	
	function ajax_delete($id) {
    $date = new DateTime();

    $data = array(
      'updated_at' => $date->getTimestamp(),
      'has_delete' => 1
    );

    $this->core_model->delete($this->table, array($this->idq => $id), $data);
    echo json_encode(array('status' => true));
	}
}

/* End of file Crud.php */
