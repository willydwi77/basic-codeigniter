<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	protected $data = array();

	public function __construct() {
		parent::__construct();

		$this->data['title']			  = 'Basic Codeigniter';
		$this->data['brand']				= 'Basic CI';
		$this->data['version']			= '<b>Version</b> 3.1.11';
		$this->data['copyright']		= '<strong>Copyright &copy; 2020 by Willy Sudwihartono</strong>. All Rights Reserved.';
	}

	/**
	 * @param string     $content
	 * @param string     $template
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	protected function render($content, $template = NULL, $data = NULL, $returnhtml = FALSE) {
		if ($template == 'json' || $this->input->is_ajax_request()) {
			header('Content-Type: application/json');
			echo json_encode($this->data);
		} elseif (is_null($template)) {
			$this->load->view($content, $this->data);
		} else {
			//  load view into data variabel with object content
			$this->data['content'] = (is_null($content)) ?'': $this->load->view($content, $this->data, TRUE);
			$this->load->view('backend/layouts/'.$template.'_view', $this->data);
		}
	}
}

// Controller for Auth right
class Auth_Controller extends MY_Controller {

	public function __construct() {
		parent::__construct();
	}

	protected function render($content = NULL, $template = 'auth', $data = NULL, $returnhtml = FALSE) {
		parent::render($content, $template);
	}
}	

// Controller for Users right
class Users_Controller extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('core_model');
		

		// Condition where the user is not logged in
		if (!$this->ion_auth->logged_in()) {
			redirect('auth', 'refresh');
		}
    
    // ambil data user yang login
		$this->data['current_user'] = $this->ion_auth->user()->row();
		
		// data statistik
		$this->data['data_user']	= 'Users';
		$this->data['total_user'] = $this->core_model->total_user();
		$this->data['data_group']	= 'Groups';
		$this->data['total_group'] = $this->core_model->total_group();
	}

	/**
	 * This function is part of ion-auth, I put here because users and groups function
	 * I cut and paste into separate folder so, for all function in ion-auth work I put this function
	 * to MY_Controller file.
	 * 
	 * @return bool Whether the posted CSRF token matches
	 */
	public function _valid_csrf_nonce(){
		$csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
		if ($csrfkey && $csrfkey === $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
			return FALSE;
	}

	/**
	 * This function is part of ion-auth, I put here because users and groups function
	 * I cut and paste into separate folder so, for all function in ion-auth work I put this function
	 * to MY_Controller file.
	 * 
	 * @return array A CSRF key-value pair
	 */
	public function _get_csrf_nonce() {
		$this->load->helper('string');
		$key = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return [$key => $value];
	}

	protected function render($content = NULL, $template = 'main', $data = NULL, $returnhtml = FALSE) {
		parent::render($content, $template);
	}
}
