# Basic Codeigniter 3.1.11 + Ion Auth + AdminLTE 3.0.5

Kerangka kerja dasar Codeigniter yang sudah terintegrasi dengan Ion-Auth sebagai autentikasi, serta Admin LTE 3 sebagai basic template, dan semua sudah menggunakan JQuery + Ajax sebagai request ke server.

Berikut framework dan plugins yang digunakan:

- AdminLTE 3.0.5
- Bootstrap 4.3.1
- Chart.js 2.9.3
- Codeigniter 3.1.11
- DataTables 1.10.20
- DateRangePicker 3.0.5
- FontAwesome Free 5.11.2
- jQuery 3.4.1
- MomentJS
- Overlay Scrollbars
- SweetAlert 2

## Notice:

**1. Autoload dan Route**
Librari yang otomatis dipanggil:

```php
  $autoload['libraries'] = array('database','session','ion_auth','form_validation');

  $autoload['helper'] = array('url','file','language','form');
```

Default route:

```php
  $route['default_controller'] = 'auth';
```

**2. application/core/MY_Controller.php**
Pembuatan parent controller yang nantinya akan di extends oleh child controllers, dimana terdapat fungsi utama
untuk me-render halaman, dsb. Jadi penggunaan untuk setiap controller yang akan dibuat nantinya akan di extend dengan controller yang ada dalam MY_Controller bukan di extend lewat CI_Controller langsung. Extend CI_Controller hanya digunakan sekali di MY_Controller.

- class MY_Controller extends CI_Controller {} parent
Core atau inti dari controller yang digunakan pada file project

```php
class MY_Controller extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	/**
	 * @param string     $content
	 * @param string     $template
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	protected function render($content, $template = NULL, $data = NULL, $returnhtml = FALSE) {
		if ($template == 'json' || $this->input->is_ajax_request()) {
			header('Content-Type: application/json');
			echo json_encode($this->data);
		} elseif (is_null($template)) {
			$this->load->view($content, $this->data);
		} else {
			//  load view into data variabel with object content
			$this->data['content'] = (is_null($content)) ?'': $this->load->view($content, $this->data, TRUE);
			$this->load->view('backend/layouts/'.$template.'_view', $this->data);
		}
	}
}
```

- class Auth_Controller extends MY_Controller {} child
Auth_Controller akan di extend oleh child controller yang mengatur segala autentikasi, contoh pada file project disini adalah class Auth

```php
class Auth_Controller extends MY_Controller {

	public function __construct() {
		parent::__construct();
	}

	protected function render($content = NULL, $template = 'auth', $data = NULL, $returnhtml = FALSE) {
		parent::render($content, $template);
	}
}
```

- class Users_Controller extends MY_Controller {} child
Users_Controller akan di extend oleh child controller lainnya, contoh pada file project disini adalah class Admin, Crud, User, Profile dan Group

```php
class Users_Controller extends MY_Controller {

	public function __construct() {
		parent::__construct();

		// Condition where the user is not logged in
		if (!$this->ion_auth->logged_in()) {
			redirect('auth', 'refresh');
		}
	}

	protected function render($content = NULL, $template = 'main', $data = NULL, $returnhtml = FALSE) {
		parent::render($content, $template);
	}
}
```

Template yang dibutuhkan untuk render backend di folder **views/backend/layouts/** dan template untuk frontend ditempatkan di **views/frontend/layouts/**

**3. Susunan file Ion-Auth Controller**
Perubahan terdapat pada Controller Ion-Auth, dimana fungsi user dan group dipisahkan dari keseluruhan file dalam Auth.php bawaan, serta dibuat untuk bisa menghandle request Ajax. Namun sebagian fungsi bawaan Ion_auth dihapus karena tidak terlalu dibutuhkan pada file project ini. Jika memang perlu silahkan download di sini [Ion_Auth 3](https://github.com/benedmunds/CodeIgniter-Ion-Auth/zipball/3)

Susunan terbaru menjadi:
- Auth Controller
- Groups Controller
- Users Controller

